##№
# Variables
##№

variable project {
  description = "Project ID"
}

variable region {
  description = "Region"
  default     = "europe-west4"
}
variable location {
  description = "location"
  default     = "europe-west4-b"
}

variable cluster_name {
  description = "cluster_name"
}


variable initial_node_count {
  default = 2
}

variable autoscaling_min_node_count {
  default = 1
}

variable autoscaling_max_node_count {
  default = 2
}

variable disk_size_gb {
  default = 100
}

variable cluster_k8s_version {
    default =  "1.17.14-gke.400"
}

variable disk_type {
  default = "pd-standard"
}

variable machine_type {
  default = "e2-medium"
}
