terraform {
  required_version = "~> 0.13"
}

# Google Cloud Platform
provider "google" {
  project = var.project
  region  = var.region
}
