for ns in $(helm ls -A | grep -v NAME | awk '{print $2}'); do
  for release in $(helm ls -n $ns| grep -v NAME | awk '{print $1}'); do
    helm get manifest $release -n $ns > $release.yaml
  done;
done;
