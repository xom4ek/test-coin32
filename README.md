
# YAML’ы устанавливаемых в кластер ресурсов.
[yamls into cluster](./docs/yamls/)

# Команду для создания нагрузки на приложение.
```sh
export HOST_TO_LOAD=grafana
export INGRESS_BASE_DOMAIN=35.204.254.109.nip.io ###ip_to_ingress(inconstant - change everytime when cluster recreate)
while true; do printf "GET /randomstring HTTP/1.0\r\nHost: $HOST_TO_LOAD.$INGRESS_BASE_DOMAIN\r\n\r\n" | nc $HOST_TO_LOAD.$INGRESS_BASE_DOMAIN 80; done;
```

<p align="left">
    <img src="./docs/load.png" alt="load"  width="650" height="300">
</p>

# Скриншот графика со сработавшим алертом и его определением.

<p align="left">
    <img src="./docs/alert_graph_grafana.png" alt="alert_graph"  width="650" height="300">
</p>
<p align="left">
    <img src="./docs/alert_def_grafana.png" alt="alert_def"  width="650" height="180">
</p>
<p align="left">
    <img src="./docs/alert_def_prom.png" alt="alert_def"  width="650" height="180">
</p>
